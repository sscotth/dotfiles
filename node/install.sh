#!/bin/sh

echo "  Installing or updating nvm and apps"
curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash

# Activate nvm
. ~/.nvm/nvm.sh

nvm install v0.10
npm install npm -g

nvm install v4.2
npm install npm -g
nvm alias lts v4.2

nvm install stable
npm install npm -g
nvm alias default stable

# Init default settings
npm config set init-license MIT

# Install common global npm applications
npm install -g bower
npm install -g cordova
npm install -g eslint
npm install -g grunt-cli
npm install -g gulp
npm install -g http-server
npm install -g ionic
npm install -g jshint
npm install -g kill-tabs
npm install -g nodemon
npm install -g pm2
npm install -g trash
