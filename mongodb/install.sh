#!/bin/sh

if test ! $(which mongo); then
  echo "  Installing MongoDB for you."
  brew install mongo
fi

mkdir -p /data/db
chown -R `whoami` /data
