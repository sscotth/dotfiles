#!/bin/sh
#
# ZSH
#
# This installs zsh and oh-my-zsh
#
echo "ZSH Bootstrap"

if [[ -n "$PASSWD" ]] && [ ! -d $HOME/.oh-my-zsh ] && [ $(xcode-select -p &> /dev/null; printf $?) -eq 0 ]; then
  echo "Installing Oh-My-Zsh"

  CR='\r'
  rm -rf ~/omz
  mkdir -p ~/omz
  curl -L https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -o ~/omz/omz-install.sh
  echo "#!/usr/bin/expect -f\nspawn sh ${HOME}/omz/omz-install.sh\nexpect \"Password for\"\nsend \"${PASSWD}\\${CR}\"\nexpect eof" > ~/omz/omz.expect
  expect ~/omz/omz.expect
  rm -rf ~/omz
  sleep 5s
elif [ $(xcode-select -p &> /dev/null; printf $?) -ne 0 ]; then
  echo "Install Xcode command line tools"
elif [ -d $HOME/.oh-my-zsh ]; then
  echo "Oh-My-Zsh Already installed"
else
  echo "Missing Password"
fi
