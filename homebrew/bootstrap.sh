#!/bin/sh

echo "Homebrew Bootstrap"

if [ ! $(which brew) ] && [ $(xcode-select -p &> /dev/null; printf $?) -eq 0 ]; then
  echo "Installing Homebrew"
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
elif [ $(xcode-select -p &> /dev/null; printf $?) -ne 0 ]; then
  echo "Install Xcode command line tools"
else
  echo "Homebrew Already installed"
fi
