#!/bin/sh

if test ! $(which chruby)
then
  echo "  Installing chruby"
  brew install chruby
fi

if test ! $(which ruby-install)
then
  echo "  Installing ruby-install"
  brew install ruby-install --HEAD
fi

echo '\nif [ -n "$BASH_VERSION" ] || [ -n "$ZSH_VERSION" ]; then\n  source /usr/local/share/chruby/chruby.sh\n  source /usr/local/share/chruby/auto.sh\nfi' >> ~/.zshrc
source /usr/local/share/chruby/chruby.sh

ruby-install ruby
chruby ruby

# Install common global ruby applications
gem install travis
gem install bundler
gem install rails
