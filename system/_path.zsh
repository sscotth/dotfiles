export PATH="./bin:/usr/local/opt/coreutils/libexec/gnubin:/usr/local/bin:/usr/local/sbin:$ZSH/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/X11/bin"
export MANPATH="/usr/local/opt/coreutils/libexec/gnuman:/usr/local/man:/usr/local/mysql/man:/usr/local/git/man"

# Add Postgres.app command line utils e.g. "psql" to path
# Must have been run at least once and moved to /Applications before it will work
if [ ! -d /Applications/Postgres.app ]; then
  cd /Applications/Postgres.app/Contents/Versions/*/bin
  export PATH=$PATH:`pwd`
  cd
fi
